package com.softtec.scenichiking.inject.component

import com.softtec.scenichiking.features.main.MainActivityViewModel
import com.softtec.scenichiking.inject.module.ModelManagerModule
import com.softtec.scenichiking.features.markerlist.MarkerListActivityViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ModelManagerModule::class])
interface ModelManagerComponent {
    fun inject(markerListActivityViewModel: MarkerListActivityViewModel)
    fun inject(mainActivityViewModel: MainActivityViewModel)
}
