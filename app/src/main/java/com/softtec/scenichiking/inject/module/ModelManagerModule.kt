package com.softtec.scenichiking.inject.module

import com.softtec.scenichiking.manager.MarkerManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ModelManagerModule {

    @Provides
    @Singleton
    fun provideCustomerModelManager(): MarkerManager {
        return MarkerManager()
    }
}