package com.softtec.scenichiking.utils

import com.google.gson.GsonBuilder
import com.softtec.scenichiking.application.ScenicHikingApplication
import java.io.IOException

class JsonLoader {

    companion object {

        fun loadJSONFromAsset(fileName: String): String? {
            var json: String?
            try {
                val `is` = ScenicHikingApplication.instance!!.assets.open(fileName)
                val size = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                json = String(buffer, Charsets.UTF_8)
            } catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }

            return json
        }
    }
}