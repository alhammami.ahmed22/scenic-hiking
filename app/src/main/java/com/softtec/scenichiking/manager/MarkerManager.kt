package com.softtec.scenichiking.manager

import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.softtec.scenichiking.utils.JsonLoader
import rx.Observable

class MarkerManager {
    var featureList: ArrayList<Feature> = ArrayList()

    fun getFeatureCollection(): Observable<List<Feature>> {
        return Observable.create { emitter ->
            if (featureList.isEmpty()) {
                val featureCollectionJson = JsonLoader.loadJSONFromAsset("features.json")
                val featureCollection = FeatureCollection.fromJson(featureCollectionJson!!)
                featureList = ArrayList(featureCollection.features())
            }

            emitter.onNext(featureList)
        }
    }

    fun updateFeatureState(position: Int, checked: Boolean) {
        featureList[position].addBooleanProperty("favourite", checked)
    }
}