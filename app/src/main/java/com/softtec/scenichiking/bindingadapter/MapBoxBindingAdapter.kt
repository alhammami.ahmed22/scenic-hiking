package com.softtec.scenichiking.bindingadapter

import androidx.databinding.BindingAdapter
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback

object MapBoxBindingAdapter {

    @JvmStatic
    @BindingAdapter(value = ["getMapAsync", "onMapReady"], requireAll = false)
    fun getMapBpxAsync(mapView: MapView, getMapAsync: Boolean, callback: OnMapReadyListener?) {
        if (getMapAsync) {
            val newValue: OnMapReadyCallback?
            if (callback != null) {
                newValue = OnMapReadyCallback { mapboxMap -> callback.onMapReady(mapboxMap) }
                mapView.getMapAsync(newValue)
            }
        }
    }

    interface OnMapReadyListener {
        fun onMapReady(mapboxMap: MapboxMap)
    }
}