package com.softtec.scenichiking.bindingadapter

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

object AndroidDataBindingAdapter {

    @JvmStatic
    @BindingAdapter(value = ["adapter", "gridLayoutManager", "reverseLayout"], requireAll = false)
    fun setRecycleViewAdapter(
        recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?, gridLayoutManager: Boolean,
        reverseLayout: Boolean
    ) {
        if (gridLayoutManager) {
            recyclerView.layoutManager = GridLayoutManager(recyclerView.context, 2)
        } else {
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context).apply {
                this.reverseLayout = reverseLayout
            }
        }
        if (recyclerView.adapter == null) {
            recyclerView.adapter = adapter
        }

        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                DividerItemDecoration.VERTICAL
            )
        )
    }
}