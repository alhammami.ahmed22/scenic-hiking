package com.softtec.scenichiking.features.main

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.softtec.scenichiking.BR

class MainActivityBindingModel : BaseObservable() {

    @Bindable
    var getAsyncMap: Boolean = false
        set(value) {
            field = value
            notifyPropertyChanged(BR.getAsyncMap)
        }
}