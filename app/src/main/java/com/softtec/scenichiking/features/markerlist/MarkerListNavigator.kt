package com.softtec.scenichiking.features.markerlist

interface MarkerListNavigator {
    fun navigateToMapActivity()
}