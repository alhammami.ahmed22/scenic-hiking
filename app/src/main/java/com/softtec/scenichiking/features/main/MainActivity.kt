package com.softtec.scenichiking.features.main

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.Mapbox
import com.softtec.scenichiking.R
import com.softtec.scenichiking.databinding.ActivityMainBinding
import android.annotation.SuppressLint
import com.mapbox.android.core.location.*
import com.mapbox.mapboxsdk.maps.MapView
import com.softtec.scenichiking.ui.MainNavigator
import java.lang.Exception

class MainActivity : AppCompatActivity(), PermissionsListener, MainNavigator {

    lateinit var viewModel: MainActivityViewModel
    lateinit var binding: ActivityMainBinding

    private val DEFAULT_INTERVAL_IN_MILLISECONDS = 6000L
    private val DEFAULT_MAX_WAIT_TIME = DEFAULT_INTERVAL_IN_MILLISECONDS * 5

    private lateinit var permissionsManager: PermissionsManager
    private lateinit var locationEngine: LocationEngine

    private val callback = MainActivityLocationCallback()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Mapbox.getInstance(this, getString(R.string.access_token))
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        viewModel = ViewModelProviders.of(this)[MainActivityViewModel::class.java]
        viewModel.navigator = this
        binding.viewmodel = viewModel
        binding.mapView.onCreate(savedInstanceState)
        viewModel.initMapView()
    }

    // Location Permissions
    override fun checkLocationPermissions() {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            viewModel.setUpMapBox()
        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String>) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG)
            .show()
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            viewModel.setUpMapBox()
        } else {
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG)
                .show()
            finish()
        }
    }

    // initLocationEngine
    @SuppressLint("MissingPermission")
    override fun initLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(this)
        val request = LocationEngineRequest.Builder(DEFAULT_INTERVAL_IN_MILLISECONDS)
            .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
            .setMaxWaitTime(DEFAULT_MAX_WAIT_TIME).build()

        locationEngine.requestLocationUpdates(request, callback, mainLooper)
        locationEngine.getLastLocation(callback)
    }

    override fun getMapView(): MapView {
        return binding.mapView
    }

    // Life Cycle
    public override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }

    override fun onStart() {
        super.onStart()
        binding.mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        binding.mapView.onStop()
    }

    public override fun onPause() {
        super.onPause()
        binding.mapView.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        binding.mapView.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.mapView.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        binding.mapView.onSaveInstanceState(outState)
    }

    inner class MainActivityLocationCallback : LocationEngineCallback<LocationEngineResult> {
        override fun onSuccess(result: LocationEngineResult?) {
            val location = result?.lastLocation ?: return
            viewModel.updateCurrentLocation(location)
        }

        override fun onFailure(exception: Exception) {
        }
    }
}
