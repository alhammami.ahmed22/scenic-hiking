package com.softtec.scenichiking.features.main

import android.app.Application
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Location
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.AndroidViewModel
import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.expressions.Expression.*
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute
import com.softtec.scenichiking.R
import com.softtec.scenichiking.application.ScenicHikingApplication
import com.softtec.scenichiking.databinding.LayoutCalloutBinding
import com.softtec.scenichiking.manager.MarkerManager
import com.softtec.scenichiking.ui.MainNavigator
import com.softtec.scenichiking.utils.SymbolGenerator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class MainActivityViewModel(application: Application) : AndroidViewModel(application),
    MapboxMap.OnMapLongClickListener, MapboxMap.OnMapClickListener {

    @Inject
    lateinit var markerManager: MarkerManager

    private val SOURCE_ID = "source-id"
    private val ICON_ID = "marker-icon-id"

    private val PROPERTY_ID = "Id"
    private val PROPERTY_SELECTED = "selected"
    private val PROPERTY_FAVOURITE = "favourite"
    private val PROPERTY_TITLE = "title"

    private val CALLOUT_LAYER_ID = "mapbox.callout"
    private val MARKER_LAYER_ID = "mapbox.marker"
    private val LINE_LAYER_ID = "mapbox.line"

    lateinit var navigator: MainNavigator
    private lateinit var mapboxMap: MapboxMap
    private var navigationMapRoute: NavigationMapRoute? = null
    private var symbolLayerIconFeatureList = ArrayList<Feature>()

    var bindingModel = MainActivityBindingModel()

    private val viewMap: MutableMap<String, LayoutCalloutBinding> = mutableMapOf()
    private var inflater = LayoutInflater.from(getCurrentContext())

    init {
        ScenicHikingApplication.instance!!.modelManagerComponent!!.inject(this)
    }

    fun initMapView() {
        bindingModel.getAsyncMap = true
    }

    fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        navigator.checkLocationPermissions()
    }

    fun setUpMapBox() {
        mapboxMap.setStyle(
            Style.Builder().fromUri(Style.MAPBOX_STREETS)
        ) {
            enableLocationComponent(it)
            setupMakerLayer(it)
            setupCalloutLayer(it)
            setUpLineLayer(it)
            setUpSource(it)
            setupClickListener()
        }
    }

    private fun setUpLineLayer(loadedMapStyle: Style) {
        loadedMapStyle.addLayer(
            LineLayer(LINE_LAYER_ID, SOURCE_ID)
                .withProperties(
                    lineCap(Property.LINE_CAP_SQUARE),
                    lineJoin(Property.LINE_JOIN_MITER),
                    lineOpacity(.7f),
                    lineWidth(7f),
                    lineColor(Color.parseColor("#3bb2d0"))
                )
        )
    }

    private fun setUpSource(loadedMapStyle: Style) {
        val geoJsonSource = GeoJsonSource(SOURCE_ID)
        loadedMapStyle.addSource(geoJsonSource)

        markerManager.getFeatureCollection()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                symbolLayerIconFeatureList.addAll(it)
                refreshResources()
                for (feature in symbolLayerIconFeatureList) {
                    addInfoLayer(feature)
                }

                val point = convertToLatLng(symbolLayerIconFeatureList.last())
                getRoute(point)
            }
    }

    @SuppressWarnings("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        val customLocationComponentOptions = LocationComponentOptions.builder(getCurrentContext())
            .trackingGesturesManagement(true)
            .accuracyColor(ContextCompat.getColor(getCurrentContext(), R.color.colorAccent))
            .build()

        val locationComponentActivationOptions =
            LocationComponentActivationOptions.builder(getCurrentContext(), loadedMapStyle)
                .locationComponentOptions(customLocationComponentOptions)
                .build()

        mapboxMap.locationComponent.apply {
            activateLocationComponent(locationComponentActivationOptions)
            isLocationComponentEnabled = true
            cameraMode = CameraMode.TRACKING
            renderMode = RenderMode.COMPASS
        }
        navigator.initLocationEngine()
    }

    private fun setupCalloutLayer(loadedMapStyle: Style) {
        loadedMapStyle.addLayer(
            SymbolLayer(CALLOUT_LAYER_ID, SOURCE_ID)
                .withProperties(
                    iconImage("{Id}"),
                    iconAnchor(Property.ICON_ANCHOR_BOTTOM_LEFT),
                    iconOffset(arrayOf(-20.0f, -10.0f))
                )
                .withFilter(eq(get(PROPERTY_SELECTED), literal(true)))
        )
    }

    private fun setupMakerLayer(loadedMapStyle: Style) {
        loadedMapStyle.addImage(
            ICON_ID,
            BitmapFactory.decodeResource(
                getCurrentContext().resources,
                R.drawable.ic_location_on
            )
        )

        val markerSymbolLayer =
            SymbolLayer(MARKER_LAYER_ID, SOURCE_ID)
                .withProperties(
                    iconImage(ICON_ID),
                    iconAllowOverlap(true),
                    iconIgnorePlacement(true),
                    iconSize(
                        match(
                            get(PROPERTY_FAVOURITE), literal(1.0f),
                            stop(true, literal(2.0f)),
                            stop(false, literal(1.0f))
                        )
                    )
                )

        loadedMapStyle.addLayer(markerSymbolLayer)
    }

    private fun setupClickListener() {
        mapboxMap.addOnMapLongClickListener(this@MainActivityViewModel)
        mapboxMap.addOnMapClickListener(this@MainActivityViewModel)
    }

    override fun onMapClick(point: LatLng): Boolean {
        val screenPoint = mapboxMap.projection.toScreenLocation(point)
        val features = mapboxMap.queryRenderedFeatures(screenPoint, MARKER_LAYER_ID)
        if (features.isNotEmpty()) {
            setSelected(features[0])
            refreshResources()
        } else {
            resetSelectedMarkers()
        }

        return true
    }

    override fun onMapLongClick(point: LatLng): Boolean {
        val destinationPoint = Point.fromLngLat(point.longitude, point.latitude)

        val feature = Feature.fromGeometry(destinationPoint)
        feature.properties()!!.addProperty(PROPERTY_ID, Date().time.toString())
        feature.properties()!!.addProperty(PROPERTY_FAVOURITE, false)
        feature.properties()!!.addProperty(PROPERTY_SELECTED, false)

        // TODO show dialog to add text
        feature.properties()!!.addProperty(PROPERTY_TITLE, "New Location added")

        symbolLayerIconFeatureList.add(feature)

        refreshResources()
        addInfoLayer(feature)
        getRoute(point)
        return true
    }

    private fun refreshResources() {
        val source = mapboxMap.style!!.getSourceAs<GeoJsonSource>(SOURCE_ID)
        source?.setGeoJson(FeatureCollection.fromFeatures(symbolLayerIconFeatureList))
    }

    private fun addInfoLayer(feature: Feature) {
        val listItemBinding: LayoutCalloutBinding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.layout_callout,
                null,
                false
            )

        val favourite = feature.getBooleanProperty(PROPERTY_FAVOURITE)
        listItemBinding.title.text = feature.getStringProperty(PROPERTY_TITLE)
        listItemBinding.logoView.setImageResource(if (favourite) R.drawable.ic_favorit_selected else R.drawable.ic_favorit_unselected)
        listItemBinding.executePendingBindings()

        mapboxMap.style?.addImage(
            feature.getStringProperty(PROPERTY_ID),
            SymbolGenerator.generate(listItemBinding.root)
        )
        viewMap[feature.getStringProperty(PROPERTY_ID)] = listItemBinding
    }

    private fun setSelected(selectedFeature: Feature) {
        for (feature in symbolLayerIconFeatureList) {
            if (feature.getProperty(PROPERTY_ID) == selectedFeature.getProperty(PROPERTY_ID)) {
                feature.properties()!!.addProperty(PROPERTY_SELECTED, true)
            } else {
                feature.properties()!!.addProperty(PROPERTY_SELECTED, false)
            }
        }
    }

    private fun resetSelectedMarkers() {
        for (feature in symbolLayerIconFeatureList) {
            feature.properties()!!.addProperty(PROPERTY_SELECTED, false)
        }
        refreshResources()
    }

    private fun getRoute(point: LatLng) {
        val navigationRouteBuilder = NavigationRoute.builder(getCurrentContext())
            .accessToken(Mapbox.getAccessToken()!!)
            .origin(getOrignePoint())
            .destination(Point.fromLngLat(point.longitude, point.latitude))
            .profile(DirectionsCriteria.PROFILE_DRIVING)

        addWayPoints(navigationRouteBuilder)

        navigationRouteBuilder.build()
            .getRoute(object : Callback<DirectionsResponse> {
                override fun onResponse(
                    call: Call<DirectionsResponse>,
                    response: Response<DirectionsResponse>
                ) {
                    if (response.body() == null) {
                        return
                    } else if (response.body()!!.routes().size < 1) {
                        return
                    }

                    val currentRoute = response.body()!!.routes()[0]

                    // Draw the route on the map
                    if (navigationMapRoute != null) {
                        navigationMapRoute!!.updateRouteVisibilityTo(false)
                    } else {
                        navigationMapRoute =
                            NavigationMapRoute(
                                null,
                                navigator.getMapView(),
                                mapboxMap,
                                R.style.NavigationMapRoute
                            )
                    }
                    navigationMapRoute!!.addRoute(currentRoute)
                }

                override fun onFailure(call: Call<DirectionsResponse>, throwable: Throwable) {
                    Timber.e("Error: %s", throwable.message)
                }
            })
    }

    private fun getOrignePoint(): Point {
        return if (symbolLayerIconFeatureList.size == 1) {
            Point.fromLngLat(
                mapboxMap.locationComponent.lastKnownLocation!!.longitude,
                mapboxMap.locationComponent.lastKnownLocation!!.latitude
            )
        } else {
            symbolLayerIconFeatureList[0].geometry() as Point
        }
    }

    private fun addWayPoints(navigationRouteBuilder: NavigationRoute.Builder) {
        if (symbolLayerIconFeatureList.size > 2) {
            for (i in 1 until symbolLayerIconFeatureList.size) {
                navigationRouteBuilder.addWaypoint(symbolLayerIconFeatureList[i].geometry() as Point)
            }
        }
    }

    fun centerMap() {
        if (symbolLayerIconFeatureList.size > 2) {
            val latLngBounds = LatLngBounds.Builder()
                .includes(getMarkersPosition())
                .build()
            mapboxMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100))
        }
    }

    private fun getMarkersPosition(): ArrayList<LatLng> {
        val markerPosition = ArrayList<LatLng>()
        for (feature in symbolLayerIconFeatureList) {
            markerPosition.add(convertToLatLng(feature))
        }

        return markerPosition
    }

    private fun convertToLatLng(feature: Feature): LatLng {
        val symbolPoint = feature.geometry() as Point
        return LatLng(symbolPoint.latitude(), symbolPoint.longitude());
    }

    fun updateCurrentLocation(location: Location) {
        mapboxMap.locationComponent.forceLocationUpdate(location)
    }

    private fun getCurrentContext(): Application {
        return getApplication()
    }
}