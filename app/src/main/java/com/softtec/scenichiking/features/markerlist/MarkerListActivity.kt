package com.softtec.scenichiking.features.markerlist

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.softtec.scenichiking.R
import com.softtec.scenichiking.databinding.ActivityMarkerlistBinding
import com.softtec.scenichiking.features.main.MainActivity

class MarkerListActivity : AppCompatActivity(), MarkerListNavigator {

    lateinit var viewModel: MarkerListActivityViewModel
    lateinit var binding: ActivityMarkerlistBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_markerlist)

        viewModel = ViewModelProviders.of(this)[MarkerListActivityViewModel::class.java]
        binding.viewModel = viewModel
        viewModel.navigator = this
        viewModel.prepareLocations()
    }

    override fun navigateToMapActivity() {
        startActivity(Intent(this, MainActivity::class.java))
    }
}
