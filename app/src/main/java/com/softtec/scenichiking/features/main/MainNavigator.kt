package com.softtec.scenichiking.ui

import com.mapbox.mapboxsdk.maps.MapView

interface MainNavigator {
    fun checkLocationPermissions()
    fun initLocationEngine()
    fun getMapView(): MapView
}