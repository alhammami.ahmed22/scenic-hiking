package com.softtec.scenichiking.features.markerlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.mapbox.geojson.Feature
import com.softtec.scenichiking.BR

open class MarkerListAdapter constructor(
    var viewModel: MarkerListActivityViewModel, var list: ArrayList<Feature>? = null,
    var layoutId: Int
) : RecyclerView.Adapter<MarkerListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater, viewType, parent, false
        )
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = getItemForPosition(position)
        holder.bind(item)
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    fun updateItem(item: Feature, position: Int) {
        list?.let {
            it[position] = item
        }
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    open fun getItemForPosition(position: Int): Feature {
        return list!![position]
    }

    open fun getLayoutIdForPosition(position: Int): Int {
        return layoutId
    }

    open fun setValues(newValues: List<Feature>?, addValues: Boolean = false) {
        if (newValues != null) {
            if (list == null) {
                list = ArrayList()
            }

            if (!addValues) {
                list!!.clear()
            }

            list!!.addAll(newValues)
            notifyDataSetChanged()
        }
    }

    inner class MyViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Feature) {
            binding.setVariable(BR.model, item)
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, adapterPosition)
            binding.executePendingBindings()
        }
    }
}