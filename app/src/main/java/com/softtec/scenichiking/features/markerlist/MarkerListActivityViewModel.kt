package com.softtec.scenichiking.features.markerlist

import android.app.Application
import android.view.View
import android.widget.ToggleButton
import androidx.lifecycle.AndroidViewModel
import com.mapbox.geojson.Feature
import com.softtec.scenichiking.R
import com.softtec.scenichiking.application.ScenicHikingApplication
import com.softtec.scenichiking.manager.MarkerManager
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class MarkerListActivityViewModel(application: Application) : AndroidViewModel(application) {

    @Inject
    lateinit var markerManager: MarkerManager

    lateinit var navigator: MarkerListNavigator
    var markerListAdapter: MarkerListAdapter

    init {
        ScenicHikingApplication.instance!!.modelManagerComponent!!.inject(this)
        markerListAdapter = MarkerListAdapter(this, ArrayList(), R.layout.item_marker)
    }

    fun prepareLocations() {
        markerManager.getFeatureCollection()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                markerListAdapter.setValues(it)
            }
    }

    fun updateStatus(feature: Feature, position: Int, view: View) {
        val checked = (view as ToggleButton).isChecked
        feature.addBooleanProperty("favourite", checked)
        markerListAdapter.updateItem(feature, position)

        // Update the manager list so we can see the value in the next activity
        markerManager.updateFeatureState(position, checked)
    }

    fun getFeatureTitle(feature: Feature): String {
        return feature.getStringProperty("title")
    }

    fun navigateToMapView() {
        navigator.navigateToMapActivity()
    }
}