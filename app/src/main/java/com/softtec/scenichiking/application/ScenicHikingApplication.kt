package com.softtec.scenichiking.application

import android.app.Application
import com.softtec.scenichiking.inject.component.DaggerModelManagerComponent
import com.softtec.scenichiking.inject.component.ModelManagerComponent

class ScenicHikingApplication : Application() {

    var modelManagerComponent: ModelManagerComponent? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
        modelManagerComponent = DaggerModelManagerComponent.builder().build()
    }

    companion object {
        var instance: ScenicHikingApplication? = null
    }
}